const { defineConfig } = require("cypress")

module.exports = defineConfig({
  chromeWebSecurity: false,
  e2e: {
    setupNodeEvents(on, config) {
      return {
        baseUrl: "https://notes-serverless-app.com",
        defaultCommandTimeout: 20000,
        env: {
          viewportWidthBreakpoint: 768
        }
      }
    }
  }
})
